.model small
.code
ORG 100h

mulai:
          jmp proses
bila db 5
vcall db 'MASSUKAN NILAI UJIAN ?$'
vcall1 db 10,'ANDA LULUS$'
vcall2 db 10,'TIDUR SAJA$'

proses:
mov ah, 09h
lea dx, vcall
int 21h

mov ah, 01h
int 21h

cmp al,53
ja tidak1
jb ya1

ya1:
mov ah, 01h
int 21h

ya2:
mov ah, 09h
lea dx, vcall2
int 21h
int 20h

tidak1:
mov ah, 01h
int 21h

tidak2:
mov ah, 09h
lea dx, vcall1
int 21h
int 20h
end mulai